"""
Note: SQLAlchemy uses the term "model" to refer to these classes and instances
that interact with the database.

But Pydantic also uses the term "model" to refer to something different, the
data validation, conversion, and documentation classes and instances.

To avoid confusion between the SQLAlchemy models and the Pydantic models, we
will have the file models.py with the SQLAlchemy models, and the file
schemas.py with the Pydantic models.

These Pydantic models define more or less a "schema" (a valid data shape).
"""

from typing import Optional, List

from datetime import datetime

from pydantic import BaseModel, HttpUrl


class User(BaseModel):
    id: int
    email: str
    enabled: bool

    class Config:
        orm_mode = True


class UserCreate(BaseModel):
    email: str
    password: str


class RunnerCreate(BaseModel):
    name: str
    owner: str
    external_id: Optional[str] = None
    description: Optional[str] = None
    architecture: Optional[str] = None
    platform: Optional[str] = None


class Runner(BaseModel):
    id: int
    name: str
    description: Optional[str] = None
    external_id: Optional[str] = None
    last_seen: Optional[datetime] = None
    architecture: Optional[str] = None
    platform: Optional[str] = None
    owner: Optional[str] = None

    class Config:
        orm_mode = True


class Job(BaseModel):
    id: int
    external_id: str
    name: str
    status: str
    stage: Optional[str] = None
    external_url: Optional[str] = None
    runner: Optional[Runner] = None
    ref: Optional[str] = None
    allow_failure: Optional[bool] = False
    commit: Optional[str] = None
    created_at: Optional[datetime] = None
    started_at: Optional[datetime] = None
    finished_at: Optional[datetime] = None
    tests_length: Optional[int] = 0

    class Config:
        orm_mode = True


class Pipeline(BaseModel):
    id: int
    external_id: str
    status: str
    sha: Optional[str] = None
    ref: Optional[str] = None
    source: Optional[str] = None
    created_at: datetime
    updated_at: Optional[datetime] = None
    external_url: Optional[str] = None
    tests_stats: Optional[dict] = {}
    jobs: List[Job]

    class Config:
        orm_mode = True


class PipelineCreate(BaseModel):
    external_id: str
    status: str
    sha: Optional[str] = None
    ref: Optional[str] = None
    source: Optional[str] = None
    created_at: datetime
    updated_at: Optional[datetime] = None
    external_url: Optional[str] = None


class TestResultAloneCreate(BaseModel):
    status: str
    execution_time: Optional[float] = None


class TestResultCreate(BaseModel):
    name: str
    class_name: str
    status: str
    weight: Optional[float] = None
    execution_time: Optional[float] = None


class JobCreate(BaseModel):
    external_id: str
    name: str
    status: str
    runner: Optional[RunnerCreate] = None
    stage: Optional[str] = None
    external_url: Optional[str] = None
    ref: Optional[str] = None
    allow_failure: Optional[bool] = False
    commit: Optional[str] = None
    created_at: Optional[datetime] = None
    started_at: Optional[datetime] = None
    finished_at: Optional[datetime] = None
    test_results: List[TestResultCreate]


class LatestPipeline(BaseModel):
    id: int
    status: str
    sha: Optional[str] = None
    ref: Optional[str] = None
    source: Optional[str] = None
    created_at: datetime
    updated_at: Optional[datetime] = None
    external_url: Optional[str] = None

    class Config:
        orm_mode = True


class Project(BaseModel):
    id: int
    name: str
    description: Optional[str] = None
    repository_url: Optional[HttpUrl] = None
    tests_length: Optional[int] = 0
    runners_length: Optional[int] = 0
    latest_pipeline: Optional[LatestPipeline] = None

    class Config:
        orm_mode = True


class ProjectCreate(BaseModel):
    name: str
    description: Optional[str] = None
    repository_url: Optional[HttpUrl] = None


class TestResult(BaseModel):
    status: Optional[str] = None
    execution_time: Optional[float] = None
    job: Optional[Job] = None

    class Config:
        orm_mode = True


class TestDetail(BaseModel):
    id: int
    name: str
    class_name: str
    weight: Optional[float]
    test_results: List[TestResult]
    results_commit_history: Optional[dict]

    class Config:
        orm_mode = True
