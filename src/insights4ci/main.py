from datetime import timedelta, date
from typing import List

from fastapi import Depends, FastAPI, HTTPException, UploadFile, File
from fastapi.middleware.cors import CORSMiddleware

from sqlalchemy.orm import Session

from insights4ci import models

from insights4ci import schemas
from insights4ci.database import engine, get_db, Base

Base.metadata.create_all(bind=engine)

app = FastAPI(title="Insights4CI API",
              contact={"name": "Insights4CI API",
                       "url": "https://gitlab.com/beraldoleal/insights4ci/",
                       "email": "bleal@redhat.com"})

ORIGINS = [
    "http://localhost:8080"
]

app.add_middleware(CORSMiddleware,
                   allow_origins=ORIGINS,
                   allow_credentials=True,
                   allow_methods=["*"],
                   allow_headers=["*"])

# TODO: We need a Class Based View or split into modules soon.


# Projects
@app.get("/projects/",
         response_model=List[schemas.Project],
         tags=['Projects'])
def list_all_projects(skip: int = 0,
                      limit: int = 100,
                      db: Session = Depends(get_db)):
    """List all projects registered."""
    return models.Project.get_all(db, skip=skip, limit=limit)


@app.post("/projects/",
          response_model=schemas.Project,
          tags=['Projects'])
def register_new_project(project: schemas.ProjectCreate,
                         db: Session = Depends(get_db)):
    """Creates a new project.

    Visit the schema `Project` for details about the fields.
    """
    db_project = models.Project.get_project_by_name(db, project.name)
    if db_project:
        msg = "Project already registered."
        raise HTTPException(status_code=400, detail=msg)
    project = models.Project.create_from_schema(db=db, schema=project)
    return project


@app.get("/projects/{project_id}",
         response_model=schemas.Project,
         tags=['Projects'])
def get_project_details(project_id: int, db: Session = Depends(get_db)):
    db_project = models.Project.get(db, id=project_id)
    if db_project is None:
        raise HTTPException(status_code=404, detail="Project not found")
    return db_project


@app.get("/runners/",
         response_model=List[schemas.Runner],
         tags=['Runners'])
def list_all_runners(skip: int = 0,
                     limit: int = 100,
                     db: Session = Depends(get_db)):
    """List all runners registered.

    Visit the schema `Runner` for details about the fields.
    """
    return models.Runner.get_all(db, skip=skip, limit=limit)


@app.post("/runners/",
          response_model=schemas.Runner,
          tags=['Runners'])
def register_new_runner(runner: schemas.RunnerCreate,
                        db: Session = Depends(get_db)):
    """Creates a new runner.

    Visit the schema `Runner` for details about the fields.
    """
    db_runner = models.Runner.get_runner_by_name(db, runner.name)
    if db_runner:
        msg = "Runner already registered."
        raise HTTPException(status_code=400, detail=msg)
    return models.Runner.create_from_schema(db=db, schema=runner)


@app.get("/runners/{runner_id}",
         response_model=schemas.Runner,
         tags=['Runners'])
def get_runner_details(runner_id: int, db: Session = Depends(get_db)):
    db_runner = models.Runner.get(db, id=runner_id)
    if db_runner is None:
        raise HTTPException(status_code=404, detail="Runner not found")
    return db_runner


@app.get("/projects/{project_id}/pipelines",
         response_model=List[schemas.Pipeline],
         tags=['Pipelines'])
def read_project_pipelines(project_id: int,
                           dt_from: date = date.today() - timedelta(days=15),
                           dt_to: date = None,
                           in_groups: str = None,
                           limit: int = 100,
                           db: Session = Depends(get_db)):
    """Read a list with latest project' pipelines.

    `dt_from`: Use a date value to inform the begin of the range. Default
               is 15 days ago.

    `dt_to`: Use a date value to inform the end of the range. Default is
             `None` which disables filtering based on this value.

    `in_groups`: If this project has test groups, you can inform a
                comma-separated string with the group ids that you would like
                to filter. By default, no filter will be applied and all
                pipeline's tests will be present. NOTE: NOT IMPLEMENTED.

    `limit`: maximum size of list range. Defaul is 100.

    Visit the schema `Pipeline` for details about the fields.
    """
    db_project = models.Project.get(db, project_id)
    if not db_project:
        msg = "Project not found."
        raise HTTPException(status_code=404, detail=msg)

    # in_groups = in_groups or None
    # try:
    #     in_groups = in_groups.split(',')
    # except AttributeError:
    #     in_groups = []
    pipelines = db_project.get_pipelines(dt_from, dt_to, limit)
    if not pipelines:
        msg = f"Project {project_id} has no pipelines."
        raise HTTPException(status_code=404, detail= msg)
    return pipelines


@app.post("/projects/{project_id}/pipelines",
          response_model=schemas.Pipeline,
          tags=['Pipelines'])
def create_pipeline(project_id: int,
                    pipeline: schemas.PipelineCreate,
                    db: Session = Depends(get_db)):
    """Creates a new pipeline for a project.

    Visit the schema `Pipeline` for details about the fields.
    """
    db_project = models.Project.get_project_by_id(db, project_id)
    if not db_project:
        msg = "Project doesn't exists. Invalid Request."
        raise HTTPException(status_code=400, detail=msg)

    try:
        return models.Pipeline.create_from_schema(db=db,
                                                  schema=pipeline,
                                                  project_id=project_id)
    except models.I4CAlreadyExists as ex:
        msg = f"The Pipeline {pipeline.external_id} already exists"
        raise HTTPException(status_code=409, detail=msg)


@app.get("/projects/{project_id}/pipelines/{pipeline_id}",
         response_model=schemas.Pipeline,
         tags=['Pipelines'])
def get_pipeline_details(project_id: int, pipeline_id: int,
                         db: Session = Depends(get_db)):
    db_project = models.Project.get_project_by_id(db, project_id)
    if not db_project:
        msg = "Project doesn't exists. Invalid Request."
        raise HTTPException(status_code=400, detail=msg)

    db_pipeline = db_project.get_pipeline_by_id(pipeline_id)
    if not db_pipeline:
        msg = "Pipeline not found."
        raise HTTPException(status_code=404, detail=msg)

    return db_pipeline


@app.get("/projects/{project_id}/jobs",
         response_model=List[schemas.Job],
         tags=['Jobs'])
def read_project_jobs(project_id: int,
                      db: Session = Depends(get_db)):
    """Read a list with latest project' jobs.

    Visit the schema `Jobs` for details about the fields.
    """
    db_project = models.Project.get(db, project_id)
    if not db_project:
        msg = "Project not found."
        raise HTTPException(status_code=404, detail=msg)
    # TODO: Limit using skip and limit
    return db_project.jobs


@app.get("/projects/{project_id}/pipelines/{pipeline_id}/jobs",
         response_model=List[schemas.Job],
         tags=['Pipelines'])
def read_pipeline_jobs(project_id: int,
                       pipeline_id: int,
                       db: Session = Depends(get_db)):
    """Read a list with the pipeline' jobs.

    Visit the schema `Jobs` for details about the fields.
    """
    db_project = models.Project.get(db, project_id)
    if not db_project:
        msg = "Project not found."
        raise HTTPException(status_code=404, detail=msg)

    db_pipeline = db_project.get_pipeline_by_id(pipeline_id)
    if not db_pipeline:
        msg = "Pipeline not found."
        raise HTTPException(status_code=404, detail=msg)

    # TODO: Limit using skip and limit
    return db_pipeline.jobs


@app.post("/projects/{project_id}/pipelines/{pipeline_id}/jobs",
          response_model=schemas.Job,
          tags=['Pipelines'])
def create_pipeline_job(project_id: int,
                        pipeline_id: int,
                        job: schemas.JobCreate,
                        db: Session = Depends(get_db)):
    """Create a single job in a pipeline.

    If a `runner` object is present, it will try to register a new Runner *only
    if* is a new runner (based on name), otherwise it will ignore this field.

    If the `runner` object is missing, a "unknown" runner it will be attached.

    Also, is most likely you would like to pass `test_results`  with a list of
    test results to be inserted along the job.
    """
    db_project = models.Project.get_project_by_id(db, project_id)
    if not db_project:
        msg = "Project doesn't exists. Invalid Request."
        raise HTTPException(status_code=400, detail=msg)

    db_pipeline = db_project.get_pipeline_by_id(pipeline_id)
    if not db_pipeline:
        msg = "Pipeline not found. Invalid Request."
        raise HTTPException(status_code=404, detail=msg)

    # TODO: Validade runner authorization here
    try:
        return models.Job.create_from_schema(db=db,
                                             schema=job,
                                             pipeline_id=pipeline_id)
    except models.I4CIntegrityError as ex:
        raise HTTPException(status_code=400, detail=ex.args)


@app.post("/projects/{project_id}/jobs/{job_id}/testresult",
          response_model=schemas.Job,
          tags=['Jobs'])
def create_test_result(project_id: int,
                       job_id: int,
                       test_result: schemas.TestResultCreate,
                       db: Session = Depends(get_db)):
    """Submit a single test result inside a job."""
    db_project = models.Project.get_project_by_id(db, project_id)
    if not db_project:
        msg = "Project doesn't exists. Invalid Request."
        raise HTTPException(status_code=400, detail=msg)

    db_job = db_project.get_job_by_id(db, job_id)
    if not db_job:
        msg = "Job not found. Invalid Request."
        raise HTTPException(status_code=404, detail=msg)

    db_test = models.Test.get_by_name_or_create(db,
                                                test_result.name,
                                                test_result.class_name,
                                                project_id,
                                                test_result.weight)

    try:
        data = {'status': test_result.status,
                'execution_time': test_result.execution_time}
        test_result = models.TestResult.create_from_dict(db,
                                                         data,
                                                         db_test.id,
                                                         db_job.id)
        return db_job
    except models.I4CIntegrityError as ex:
        raise HTTPException(status_code=400, detail=ex.args)


@app.get("/projects/{project_id}/tests",
         tags=['Tests'])
def read_project_test_summary(project_id: int,
                              dt_from: date = date.today() - timedelta(days=7),
                              dt_to: date = date.today(),
                              in_groups: str = None,
                              db: Session = Depends(get_db)):
    db_project = models.Project.get(db, project_id)
    if not db_project:
        msg = "Project not found."
        raise HTTPException(status_code=404, detail=msg)

    # in_groups = in_groups or None
    # try:
    #     in_groups = in_groups.split(',')
    # except AttributeError:
    #     in_groups = []
    return db_project.get_test_summary(dt_from, dt_to)


@app.get("/projects/{project_id}/tests/{test_id}",
         response_model=schemas.TestDetail,
         tags=['Tests'])
def read_project_test_details(project_id: int,
                              test_id: int,
                              db: Session = Depends(get_db)):
    db_project = models.Project.get(db, project_id)
    if not db_project:
        msg = "Project not found."
        raise HTTPException(status_code=404, detail=msg)
    # TODO: Limit using skip and limit

    db_test = models.Test.get(db, test_id)
    if not db_test:
        msg = "Test not found."
        raise HTTPException(status_code=404, detail=msg)
    return db_test
