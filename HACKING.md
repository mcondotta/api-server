# Hacking

First of all, thank you for contributing to this project.  Here you will find
some tips about how to setup your development environment.

## Deploying in a local container for development

### Configure the database

Make sure that you have a MariaDB server running with the proper credentials:

    database: insights4ci
    user: insigths4ci
    password: insights4ci

## Starting the environment in a container for development

Build and run the specific docker container in your local machine:

    $ docker build -t i4c-server-dev . -f Dockerfile.dev
    $ docker run --net=host \
                 -v $(pwd)/src/insights4ci:/home/api/src/insights4ci \
                 i4c-server-dev

Or alternatively, if you are using Podman:

    $ buildah bud -t i4c-server-dev Dockerfile.dev
    $ podman run -v $(pwd)/src/insights4ci:/home/api/src/insights4ci \
                 i4c-server-dev

Note that the volume is needed in order to uvicorn watch changes inside
this directory.

After that, uvicorn it will start a webserver at `localhost:8080`, point it to
your browser to access the API endpoints.

## Reporting issues

If you found any issue during the bootstrap, please feel free to [Open a new
issue](https://gitlab.com/insights4ci/api-server/-/issues).

## Before submitting a MR

Make sure you have read our Contributor Guide at
[CONTRIBUTING.md](CONTRIBUTING.md), before sending a MR.
