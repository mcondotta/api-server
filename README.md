# Insights4CI - Server side

This directory is holding a FastAPI project with some basic REST API. The 
server component will handle only the API requests, leaving the frontend views
to VueJS (under the [web-ui](https://gitlab.com/insights4ci/web-ui)
repository).

Keep in mind that this is a project in the initial stage of development; there
is no authentication/authorization. It would be best not to expose this API or
the database outside your trusted network.

## Installing

### Production deployment

When deploying to production, please use `Dockerfile` that is prepared to build
this app optimized for "production".

You can deploy it in your OCP or K8S cluster with the following command:

First, deploy a mariadb service:

```
$ oc new-app mariadb
```

This will deploy a mariadb database with random password. Extract the secrets
so you can use as argument for the api-server:

```
$ oc extract secrets mariadb
```

At this point, some files will be saved in your current folder, with database,
username and password that you can use during the following steps.

**Note:** For now, this database is ephemeral and all data will be destroyed.
TODO: Add a persistent storage here.

Deploy the api-server app, passing the database settings:

```
$ oc new-app https://gitlab.com/insights4ci/api-server \
             --name=i4c-api-server \
             --strategy=docker \
             -e DATABASE_URL="://user:password@mariadb:3306/sampledb"
```

Now, expose this service to the world, so you can point it to your web browser:

```
$ oc expose svc/i4c-api-server
route.route.openshift.io/i4c-api-server exposed
```

### Development deployment

If you would like to contribute with this project, it is better to have it
running locally with debug mode, development requirements installed and "watch
feature" enabled.

Visit [HACKING.md](HACKING.md) and [CONTRIBUTING.md](CONTRIBUTING.md) files for
details.

## Accessing the API

FastAPI it will be listening on port 8000, however after exposing your service,
a redirect it was created for you.

You can use `oc status` to see the status of your deployment and the
public address of this service. After the build is completed your pod will be
accepting requests. Just point the address to your browser. You can access the
documentation in two different views:

 * http://your-cluster-address/docs
 * http://your-cluster-address/redoc

The first one you have the possibility to call the methods directly from the
browser.

**Note:** Save the routed address above to properly set your `I4C_API_SERVER`
build variable when deploying the [web-ui
project](https://gitlab.com/insights4ci/web-ui).
